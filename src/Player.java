import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


public class Player {
	int playerId;
	int baseScore;
	int gamePosition;
	int avgGainScore;
	int multipleScoreBetForPlayer;
	int playerScoreGainForGamePlay;
	String playerResult;
	Boolean lateStatus;
	
	public Player(int playerId, int baseScore, int gamePosition,
			int avgGainScore, int multipleScoreBetForPlayer,String playerResult,Boolean isLate) {
		super();
		this.playerId = playerId;
		this.baseScore = baseScore;
		this.gamePosition = gamePosition;
		this.avgGainScore = avgGainScore;
		this.multipleScoreBetForPlayer = multipleScoreBetForPlayer;
		this.playerResult = playerResult;
		this.lateStatus = isLate;
	}
	
	public void setPlayerResult(String result){
		this.playerResult = result;
	}
	
	public void setPlayerParticipantNotPlayScore(){
		this.playerScoreGainForGamePlay = 0;
		System.out.println("The gamePlay gain for each not play player: " + this.playerScoreGainForGamePlay);
	}
	
	public void setPlayerAbsentScore(int gameTypeScore){
		this.playerScoreGainForGamePlay = -(int) gameTypeScore; 
		System.out.println("The gamePlay gain for each absent player: " + this.playerScoreGainForGamePlay);
	}
	
	public void setBaseScore(){
		this.baseScore = this.baseScore+this.playerScoreGainForGamePlay;
	}
	
	public void setPlayerScoreGainForGamePlay(JsonArray tournamentIndividualScheme, ArrayList<Player> playerList, int tournamentParameter, int bonusFromTeam,int gamePlayTypeScore){
		this.playerScoreGainForGamePlay = finalScoreGain(tournamentIndividualScheme,playerList,tournamentParameter, bonusFromTeam,gamePlayTypeScore);
		System.out.println("The gamePlay gain for each player: " + this.playerScoreGainForGamePlay);
	}
	
	public int finalScoreGain(JsonArray tournamentIndividualScheme, ArrayList<Player> playerList, int tournamentParameter, int bounsFromTeam, int gamePlayTypeScore){
		JsonObject individualScoreGainScheme = Util.getScoreGainScheme(this.baseScore, tournamentIndividualScheme);
		int teamBonus = 0;
		
		//based On Scheme
		int scoreBasedOnScheme = individualScoreGainScheme.get(this.playerResult).getAsInt();
		
		//based On multipleScoreBetForPlayer
		int scoreAddOnmultiple = this.multipleScoreBetForPlayer* scoreBasedOnScheme;
		
		//based on late for game
		int latePenalty = 0;
		if(this.lateStatus== true && this.baseScore>800){
			latePenalty = -gamePlayTypeScore;
		}
		
		//based On player average score gain
		int scoreAddOnAverage = scoreAddOnmultiple + this.avgGainScore;
		int scoreAddOnBonus = scoreAddOnAverage + bounsFromTeam;
		int addOnParament = (int) (tournamentParameter + gamePlayTypeScore +latePenalty);
		
		//based On Team Position Bonus
		for(int i =0; i< playerList.size(); i++){
			if(this == playerList.get(i)){
				continue;
			}
			teamBonus = this.checkTeamBonusGain(playerList.get(i).gamePosition);
			if(teamBonus > 0){
				break;
			}
		}
		return scoreAddOnBonus + addOnParament +teamBonus; 
	}
	
	// check the team member position >6 score gain
	public int checkTeamBonusGain(int otherTeamMemberPosition){
		int gapPosition = Math.abs(this.gamePosition - otherTeamMemberPosition);
		if(gapPosition>= 6){
			return 2;
		}else{
			return 0;
		}
	}
}
