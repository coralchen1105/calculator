import java.io.FileReader;
import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class Util {
	
	public static JsonObject readJsonFile(String url){
		JsonElement je = null;
		try{
			JsonParser parser =new JsonParser();	
			je = parser.parse(new FileReader(url));
		}catch(Exception e){
		}
		return je.getAsJsonObject();
	}
	
	// build up one gamePlay general info
	public static GamePlay initGeneralGamplayInfo(JsonObject tournamentInput){
		int tournamentId = tournamentInput.get("tournamentId").getAsInt();
		int totalPlayerNumber = tournamentInput.get("totalPlayerNumber").getAsInt();
		int totalTeamNumber = tournamentInput.get("totalTeamNumber").getAsInt();
		Tournament tournament = new Tournament(tournamentId, totalPlayerNumber, totalTeamNumber);
		tournament.setTournamentTeamScheme();
		tournament.setTournamentIndividualScheme();
		String gameType = tournamentInput.get("gameType").getAsString();
		int numberOfPlayerRequired = tournamentInput.get("numberOfPlyaerReqiured").getAsInt();
		GameSpec gameSpec = new GameSpec(gameType,numberOfPlayerRequired);
		gameSpec.setGameTypeScore();
		
		GamePlay gamePlay = new GamePlay(tournament,gameSpec);
		return gamePlay;
	}
	
	public static Team teamInit(JsonObject jo){
		int teamId = jo.get("teamId").getAsInt();
		int teamBaseScore = jo.get("teamBaseScore").getAsInt();
		int teamPosition = jo.get("previousTeamPosition").getAsInt();
		int multipleScoreBetForTeam = jo.get("multipleScoreBetForTeam").getAsInt();
		String teamResult = jo.get("result").getAsString();
		JsonArray playerArray = jo.get("Player").getAsJsonArray();
		
		Team team = new Team();
		ArrayList<Player> playerList = new ArrayList<Player>(); 
		
		for(int i=0; i< playerArray.size();i++){
			int playerId = playerArray.get(i).getAsJsonObject().get("playerId").getAsInt();
			int baseScore = playerArray.get(i).getAsJsonObject().get("baseScore").getAsInt();
			int gamePosition = playerArray.get(i).getAsJsonObject().get("previousGamePosition").getAsInt();
			int avgGainScore = playerArray.get(i).getAsJsonObject().get("avgGainScore").getAsInt();
			int multipleScoreBetForPlayer = playerArray.get(i).getAsJsonObject().get("multipleScoreBetForPlayer").getAsInt();
			String playerResult = teamResult;
			Boolean isLate = playerArray.get(i).getAsJsonObject().get("islate").getAsBoolean();
			playerList.add(new Player(playerId, baseScore, gamePosition, avgGainScore, multipleScoreBetForPlayer,playerResult,isLate));
		}
		
		int teamAvgScore = team.getTeamPlayerAvgScore(playerList);
		team.setTeam(teamId, playerList, teamBaseScore, teamPosition, multipleScoreBetForTeam, teamAvgScore,teamResult);;
		
		return team;
	}
	
	public static JsonObject lookForVerseTeam(JsonArray teamArray, int oppositionTeamId){
		JsonObject verseTeamObject = new JsonObject();
		for(int i=0; i<teamArray.size();i++){
			if(teamArray.get(i).getAsJsonObject().get("teamId").getAsInt() == oppositionTeamId){
				verseTeamObject = teamArray.get(i).getAsJsonObject();
				break;
			}
		}
		return verseTeamObject;
	}
	
	public static void calculatorScore(GamePlay gamePlay){
		//based on team position gap
		int tournamentGovernmentParameter = gamePlay.tournament.totalTeamNumber - gamePlay.oppositionTeam.teamPosition;
		int tournamentOppositionParameter = gamePlay.tournament.totalTeamNumber - gamePlay.governmentTeam.teamPosition;
		
		int gamePlayTypeScore =gamePlay.gameSpec.gameTypeScore;
		
		// get the gap of two team members avg score gap
		gamePlay.governmentTeam.setScoreForPlayerBasedOnTeamScoreGap(gamePlay.oppositionTeam.teamAvgScore);
		gamePlay.oppositionTeam.setScoreForPlayerBasedOnTeamScoreGap(gamePlay.governmentTeam.teamAvgScore);
		
		// set up the score gained by each team
		gamePlay.governmentTeam.setTeamScoreGainForGamePlay(gamePlay.tournament.tournamentTeamScheme, gamePlay.oppositionTeam.teamBaseScore,tournamentGovernmentParameter, gamePlayTypeScore);
		gamePlay.oppositionTeam.setTeamScoreGainForGamePlay(gamePlay.tournament.tournamentTeamScheme, gamePlay.governmentTeam.teamBaseScore,tournamentOppositionParameter,gamePlayTypeScore);
		
		gamePlay.governmentTeam.setTeamBaseScore();
		gamePlay.oppositionTeam.setTeamBaseScore();
		
		for(int i= 0;i<gamePlay.governmentTeam.teamPlayerList.size();i++){
			gamePlay.governmentTeam.teamPlayerList.get(i).setPlayerScoreGainForGamePlay(gamePlay.tournament.tournamentIndividualScheme, gamePlay.governmentTeam.teamPlayerList,tournamentGovernmentParameter, gamePlay.governmentTeam.scoreForPlayerBasedOnTeamScoreGap,gamePlayTypeScore);
			gamePlay.governmentTeam.teamPlayerList.get(i).setBaseScore();
		}
		
		for(int i= 0;i<gamePlay.oppositionTeam.teamPlayerList.size();i++){
			gamePlay.oppositionTeam.teamPlayerList.get(i).setPlayerScoreGainForGamePlay(gamePlay.tournament.tournamentIndividualScheme,gamePlay.oppositionTeam.teamPlayerList,tournamentOppositionParameter,gamePlay.oppositionTeam.scoreForPlayerBasedOnTeamScoreGap,gamePlayTypeScore);
			gamePlay.oppositionTeam.teamPlayerList.get(i).setBaseScore();
		}	
	}
	
	public static void calculatorParticipantScore(GamePlay gamePlay){
		Team governmentTeam = gamePlay.getGamePlayGovernmentTeam();
		ArrayList<Player> playerList = governmentTeam.getPlayerList();
		for(int i=0; i<playerList.size();i++){
			playerList.get(i).setPlayerParticipantNotPlayScore();
		}
	}
	
	public static void calculatorAbsentScore(GamePlay gamePlay){
		Team governmentTeam = gamePlay.getGamePlayGovernmentTeam();
		ArrayList<Player> playerList = governmentTeam.getPlayerList();
		for(int i=0; i<playerList.size();i++){
			if(playerList.get(i).baseScore >800){
				playerList.get(i).setPlayerAbsentScore(gamePlay.gameSpec.gameTypeScore);
			}else{
				playerList.get(i).setPlayerParticipantNotPlayScore();
			}
			
		}
	}
	
	public static JsonObject getScoreGainScheme(int BaseScore,
			JsonArray tournamentScheme){
		
		JsonObject scoreGainObject = new JsonObject();
		for(int i=0; i< tournamentScheme.size();i++){
			JsonObject eachScoreScheme = (JsonObject) tournamentScheme.get(i);
			int min = eachScoreScheme.get("minBaseScore").getAsInt();
			int max = eachScoreScheme.get("maxBaseScore").getAsInt();
			
			if((BaseScore<= max) && (BaseScore>=min)){
				scoreGainObject = eachScoreScheme.get("scoreGain").getAsJsonObject();
				break;
			}
		}
		return scoreGainObject;
	}
}
