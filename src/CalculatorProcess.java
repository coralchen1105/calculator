import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


public class CalculatorProcess {
	
	public static void main(String[] args) throws IOException{
		
		ArrayList<GamePlay> gamePlayList = new ArrayList<GamePlay>();
		JsonObject tournamentInput =  Util.readJsonFile("src/gamePlay-input-version2.json");
		JsonArray teamArray = (JsonArray) tournamentInput.get("Team");
		System.out.println(teamArray.size());

		while(teamArray.size()!= 0){
			for(int i=0; i< teamArray.size();i++){
				System.out.println(i);
				GamePlay gamePlay = Util.initGeneralGamplayInfo(tournamentInput);
				JsonObject jo = (JsonObject) teamArray.get(i);
				System.out.println(jo.get("teamId"));
				
				if((jo.get("verse").getAsInt()== 0) && (jo.get("result").getAsString().equals("NA"))){
					// show up but not join match
					Team governmentTeam = Util.teamInit(jo);
					gamePlay.setGamePlayForTeamWithoutVerseTeam(governmentTeam);
					Util.calculatorParticipantScore(gamePlay);
					gamePlayList.add(gamePlay);
					teamArray.remove(i);
					break;
					
				}else if((jo.get("verse").getAsInt()==-1) && (jo.get("result").getAsString().equals("NA"))){
					// absent team
					Team governmentTeam = Util.teamInit(jo);
					gamePlay.setGamePlayForTeamWithoutVerseTeam(governmentTeam);
					Util.calculatorAbsentScore(gamePlay);
					gamePlayList.add(gamePlay);
					teamArray.remove(i);
					break;
					
				}else if((jo.get("verse").getAsInt()>0) && !(jo.get("result").getAsString()=="NA")){
					// team show up and join match
					Team governmentTeam = Util.teamInit(jo);
					int opppsitionTeamId = teamArray.get(i).getAsJsonObject().get("verse").getAsInt();
					JsonObject verseTeamObject = Util.lookForVerseTeam(teamArray, opppsitionTeamId);
					Team oppositionTeam = Util.teamInit(verseTeamObject);
					gamePlay.setGamePlayTeam(governmentTeam, oppositionTeam);
					Util.calculatorScore(gamePlay);
					
					gamePlayList.add(gamePlay);
					teamArray.remove(i);
					teamArray.remove(verseTeamObject);
					break;
				}
			}
			System.out.println(gamePlayList.size());
			
		}
		
		Gson gson = new Gson();
        String json = gson.toJson(gamePlayList);
        
        FileWriter file = new FileWriter("src/gamePlay-output.json");
        try{
             file.write(json);
        }catch(IOException e){
        	e.printStackTrace();
        }finally{
        	file.flush();
            file.close();
        }
       
        System.out.println(json);
	}
}
